
import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Pagina de Avatars'),
        ),
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/avatar.png'),
              Text('Estas en la Pagina de Avatars'),
              Icon(Icons.accessibility)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton (
          onPressed: () {Navigator.pop(context);},
          child :  Icon(Icons.arrow_back),
        ),
    );
  }
}