
import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Pagina de Cards'),
        ),
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/card.png'),
              Text('Estas en la Pagina de Cards'),
              Icon(Icons.folder_open)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton (
          onPressed: () {Navigator.pop(context);},
          child :  Icon(Icons.arrow_back),
        ),
    );
  }
}