import 'package:flutter/material.dart';

/////////////////////////////////Lab 12 hasta punto 1.5
/* 
class HomePageTemp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
        ),
        body: Text(
          'Hola Amigos',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.blue, fontSize: 25.0),
        ));
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 2.2
/* 
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: Align(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Uno', style: styleText),
              Text('Dos', style: styleText),
              Text('Tres', style: styleText),
            ],
          ),
        ),
      );
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 3.2
/* 
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlutterLogo(size: 60.0,),
              FlutterLogo(size: 60.0,),
              FlutterLogo(size: 60.0,),
            ],
          ),
        ),
      );
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 4.2
/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
             Icon(Icons.adb, color: Colors.red, size: 50.0),
             Icon(
               Icons.beach_access,
               color: Colors.blue,
               size: 36.0,
             ),
            ],
          ),
        ),
      );
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 5.4
/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
             Image.asset('assets/images/Deadpool.png'),
             Icon(
               Icons.beach_access,
               color: Colors.blue,
               size: 36.0,
             ),
            ],
          ),
        ),
      );
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 6.2
/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(title: Text('Uno')),
            ListTile(title: Text('Dos')),
            ListTile(title: Text('Tres')),
          ],
        )
      );
  }
}
*/

/////////////////////////////////Lab 12 hasta punto 6.4
/*
class HomePageTemp extends StatelessWidget {
  final options = ['Uno', 'Dos', 'Tres', 'Cuatro','Cinco' ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: ListView(
          children: _crearItemsCortal()
        )
      );
  }

  List<Widget> _crearItemsCortal() {
    return options.map((item) {
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier cosa')),
          Divider()
        ],
      );
    }).toList();
  }
}
*/

/////////////////////////////////Lab 12 TAREA

class HomePageTemp extends StatelessWidget {
  Icon flechita = Icon(Icons.keyboard_arrow_right);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Componentes'),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.notifications),
              trailing: flechita,
              title: Text('Alertas')
            ),
            ListTile(
              leading: Icon(Icons.accessibility),
              trailing: flechita,
              title: Text('Avatars')
            ),
            ListTile(
              leading: Icon(Icons.credit_card),
              trailing: flechita,
              title: Text('Cards - Tarjetas')
            ),
            ListTile(
              leading: Icon(Icons.donut_large),
              trailing: flechita,
              title: Text('Animated Container')
            ),
            ListTile(
              leading: Icon(Icons.input),
              trailing: flechita,
              title: Text('Inputs')
            ),
            ListTile(
              leading: Icon(Icons.sort),
              trailing: flechita,
              title: Text('Slider - Checks')
            ),
            ListTile(
              leading: Icon(Icons.format_list_bulleted),
              trailing: flechita,
              title: Text('Listas y Scroll')
            ),
          ],
        ),
      );
  }
}
