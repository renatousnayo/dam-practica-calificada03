import 'package:flutter/material.dart';

class ListasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Pagina Listas'),
        ),
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/lista.png'),
              Text('Estas en la Pagina de Lista'),
              Icon(Icons.format_list_bulleted)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton (
          onPressed: () {Navigator.pop(context);},
          child :  Icon(Icons.arrow_back),
        ),
    );
  }
}